<?php
require_once('animal.php');
require_once('frog.php');
require_once('ape.php');
$sheep = new Animal("shaun");

echo "Nama Hewan: " . $sheep->name . "<br>"; // "shaun"
echo "Jumlah Kaki : " . $sheep->legs . "<br>"; // 4
echo "Hewan Berdarah Dingin : " . $sheep->cold_blooded . "<br> <br>"; // "no"

$frog = new Frog("frog");
echo "Nama Hewan: " . $frog->name . "<br>"; // "shaun"
echo "Jumlah Kaki : " . $frog->legs . "<br>"; // 4
echo "Hewan Berdarah Dingin : " . $frog->cold_blooded . "<br>"; // "no"
echo "Jump : " . $frog->jump() . " <br> <br>";

$ape = new Ape("ape");
echo "Nama Hewan: " . $ape->name . "<br>"; // "shaun"
echo "Jumlah Kaki : " . $ape->legs . "<br>"; // 4
echo "Hewan Berdarah Dingin : " . $ape->cold_blooded . "<br>"; // "no"
echo "Yell : "  . $ape->yell();

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())
